package co.micromacro.nfctrack

class Coding {

	long containerId
	long targetId
	long version
	//before transformed
	long containerId0 
	long targetId0
	long version0
	int seq
	
	Long[] rotTarget = [3216549870,1234567889]
	Long[] rotContainer = [11111,54321]
	
	def Coding(String code){
		parse(code)
		process()
	}
	
	def parse(String code){
		targetId0 = Long.parseLong(code.substring(0, 10))
		containerId0 = Long.parseLong(code.substring(10, 15))
		version0 = Long.parseLong(code.substring(15))
	}
	
	
	
	def process(){
		//VaJpPCsUMdB->1234567890543210221->1234567890,54321,0221
		seq = version0 %10
		version = version0
		def  baseTarget =  (long)rotTarget[seq]
		containerId = (containerId0 - rotContainer[seq] + 100000)%100000
		targetId = (targetId0 - rotTarget[seq] + 10000000000)%10000000000
	}
	
}
