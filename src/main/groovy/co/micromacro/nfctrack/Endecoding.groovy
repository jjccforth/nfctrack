package co.micromacro.nfctrack

class Endecoding {

	def static Base62Alphabet =   'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'


	static def encodeInBase62(BigInteger number)
   {
	  // Base 62 is > 5 but < 6 bits per char.
	  def result =  StringBuilder.newInstance()//(number.bitCount() / 5 + 1)
	  def left = number
	  while(true) {
		def remainder = ((BigInteger)left) % 62
		left = (BigInteger)(left/62)
		def charecter = Base62Alphabet.getAt(remainder)
		result += charecter
		
		if(left == 0) break
	  }
	  
	  result
	}
   
   def static decodeBase62(String text){
	   BigInteger n = 0;
	   def rev = text.reverse()
	   for(int i = 0;i < rev.length(); i++){
		   n = n * 62 + Base62Alphabet.findIndexOf {it == rev.getAt(i)}
	   }
	   
	   return n;
   }
   
   def static contactSeg(long targetId,long containerId,int versionId){
	   BigInteger result = targetId*100000 + containerId
	   result = result * 10000 + versionId
	   return result
   }
   
}
