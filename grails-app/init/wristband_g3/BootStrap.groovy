package wristband_g3

import co.micromacro.nfctrack.City
import co.micromacro.nfctrack.Role
import co.micromacro.nfctrack.User
import co.micromacro.nfctrack.UserRole

class BootStrap {

    def init = { servletContext ->
        def adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true)
        def managerRole = new Role(authority: 'ROLE_MANAGER').save(flush: true)
        def userRole = new Role(authority: 'ROLE_USER').save(flush: true)
        //in case under DataSource configured as "Update", above statements will fail
        //For now use following get around tricks but it's not safe because id could be not 1,2,3
        if (User.count() == 0)
        {
            if(adminRole == null)
                adminRole = Role.get(1)
            if(managerRole == null)
                managerRole = Role.get(2)
            if(userRole == null)
                userRole = Role.get(3)

            def testUser1 = new User(username: 'user1', enabled: true, password: 'password')

            testUser1.save(flush: true,failOnError:true)
            assert testUser1 != null
            UserRole.create testUser1, adminRole, true


            def testUser2 = new User(username: 'user2', enabled: true, password: 'password')

            testUser2.save(flush: true,failOnError:true)
            assert testUser2 != null
            UserRole.create testUser2, userRole, true
            assert User.count() == 2
        }


        if(City.count() == 0){
            def testCity1 = new City(city:'Toronto')
            testCity1.save(flush:true,failonError:true)

        }

    }
    def destroy = {
    }
}
