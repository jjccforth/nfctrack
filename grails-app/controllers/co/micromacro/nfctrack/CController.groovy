package co.micromacro.nfctrack

class CController {

    def index() { }
	
	def a(String id){
		
		def targetInstance = ap(id)
		if (!targetInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'target.label', default: 'Target'), id])
			redirect(controller:"Target", action: "list")
			return
		}
		def dob = targetInstance.dateOfBirth
		def today = new Date()
		def  age = today.year - dob.year
		log.info("age:${age}")

		targetInstance.accessCount =  targetInstance.accessCount+1;
		targetInstance.save(flush: true)
		
		def view =  "a_mobile" //"a"
		withMobileDevice { device ->
			view = "a_mobile"
		 }
		render(view: view, model: [targetInstance: targetInstance, fakeId:id,age:age])
	}

	def anext(String id){
		
		def targetInstance = ap(id)
		def view = "a_mobilen" //"a"
		withMobileDevice { device ->
			view = "a_mobilen"
		 }
		render(view: view, model: [targetInstance: targetInstance,fakeId:id])
	}

	/**
	 * Get a virtual ID(Base62 encoded information) and translated into instance
	 * @param id
	 * @return
	 */
	def private Target ap(String id){
		
		if(id == null||id.isEmpty()){
			render(status: 503, text: 'Failed operation.')
			return null;
		}
		
		//def currentDevice = DeviceUtils.getCurrentDevice(servletRequest);
		
		
		BigInteger decoded = Endecoding.decodeBase62(id)
		Coding coding = new Coding(decoded.toString())
		
		//VaJpPCsUMdB
		log.info(decoded.toString())
		log.info(coding.containerId);
		log.info(coding.targetId);
		log.info(coding.version);
		

		//isMobile()
		
		Target targetInstance
		
		if(coding.seq == 0x01)
			targetInstance = Target.get(coding.targetId)
		//else Target01,Target02
			
		if (!targetInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'target.label', default: 'Target'), id])
			redirect(controller:"Target", action: "list")
			return
		}

		return targetInstance
	}
	
	
	def encodeTextWithBase62DontPad(String text)
	{
		def bytes = text.getBytes("UTF-8")
		def bigint = new BigInteger(1, bytes)
		def result = encodeInBase62(bigint)
		return result
	}
	 
	
	
	def Base62Alphabet =   'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
	
	def encodeInBase62(BigInteger number)
   {
	  // Base 62 is > 5 but < 6 bits per char.
	  def result =  StringBuilder.newInstance()//(number.bitCount() / 5 + 1)
	  def left = number
	  while(true) {
		def remainder = ((BigInteger)left) % 62
		left = (BigInteger)(left/62)
		def charecter = Base62Alphabet.getAt(remainder)
		result += charecter
		
		if(left == 0) break
	  }
	  
	  result
	}
   
   

}
