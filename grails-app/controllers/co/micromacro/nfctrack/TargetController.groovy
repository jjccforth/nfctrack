package co.micromacro.nfctrack

//import grails.plugins.springsecurity.Secured
import org.springframework.security.access.annotation.Secured

import java.security.MessageDigest

//import org.grails.plugins.imagetools.ImageTool
import org.springframework.dao.DataIntegrityViolationException



class TargetController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	private String imgFile
	private String thumbnailFile
	private String serialNo
	
	def valueBean

    def index() {
        redirect(action: "l", params: params)
    }

	@Secured(['ROLE_ADMIN','ROLE_USER','ROLE_MANAGER'])
    def l(Integer max) {
        params.max = Math.min(max ?: 10, 100)
		
		def ver = 1
		Long[] rotTarget = valueBean.rotTarget
		Long[] rotContainer = valueBean.rotContainer
		long baseT = rotTarget[ver]
		long baseC = rotContainer[ver]
		
		//Endecoding.contactSeg(baseT + )
		List<Target> listTarget = Target.list(params)
		for(int i = 0; i< listTarget.size();i++){
			BigInteger bid = Endecoding.contactSeg(baseT+listTarget[i].id, baseC+1, ver)
			log.info(bid)
			String shortened = Endecoding.encodeInBase62(bid)
			log.info(shortened)
			listTarget[i].shortCode = shortened
		}
		
        [targetInstanceList: Target.list(params), targetInstanceTotal: Target.count()]
    }
	@Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def create() {
        [targetInstance: new Target(params)]
    }
	@Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def save() {
        def targetInstance = new Target(params)
		
		upload()
		targetInstance.imgFile = imgFile
		targetInstance.accessCount = 0
		//targetInstance.serial = serialNo
		
        if (!targetInstance.save(flush: true)) {
            render(view: "create", model: [targetInstance: targetInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'target.label', default: 'Target'), targetInstance.id])
        redirect(action: "show", id: targetInstance.id)
    }

    def show(Long id) {
        def targetInstance = Target.get(id)
        if (!targetInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'target.label', default: 'Target'), id])
            redirect(action: "l")
            return
        }

        [targetInstance: targetInstance]
    }
	@Secured(['ROLE_ADMIN','ROLE_MANAGER'])
    def edit(Long id) {
        def targetInstance = Target.get(id)
        if (!targetInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'target.label', default: 'Target'), id])
            redirect(action: "l")
            return
        }

        [targetInstance: targetInstance]
    }

    def update(Long id, Long version) {
        def targetInstance = Target.get(id)
        if (!targetInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'target.label', default: 'Target'), id])
            redirect(action: "l")
            return
        }

        if (version != null) {
            if (targetInstance.version > version) {
                targetInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'target.label', default: 'Target')] as Object[],
                          "Another user has updated this Target while you were editing")
                render(view: "edit", model: [targetInstance: targetInstance])
                return
            }
        }

        targetInstance.properties = params

        if (!targetInstance.save(flush: true)) {
            render(view: "edit", model: [targetInstance: targetInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'target.label', default: '��Ա'), targetInstance.id])
        redirect(action: "show", id: targetInstance.id)
    }

    def delete(Long id) {
        def targetInstance = Target.get(id)
        if (!targetInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'target.label', default: 'Target'), id])
            redirect(action: "l")
            return
        }

        try {
            targetInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'target.label', default: 'Target'), id])
            redirect(action: "l")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'target.label', default: 'Target'), id])
            redirect(action: "show", id: id)
        }
    }
	
	def upload() {
		imgFile = null
		def f = request.getFile('myFile')
		def fo = f.getOriginalFilename()
		log.info("Original File:" + fo)
 
		if (f.empty) {
			flash.message = 'file cannot be empty'
			render(view: 'create')
			return
		}
		def ext = ""
		
		
		if (fo.endsWith(".jpg"))
			ext = ".jpg"
		else if(fo.endsWith(".png"))
			ext = ".png"
		else if(fo.endsWith(".gif"))
			ext = ".gif"
		else{
			flash.message = 'wrong file type'
			render(view: 'create')
			return
		}
 
		long now = java.lang.System.currentTimeMillis()
		log.info("Now:" + now)
		//fn =
		
		def baseFolder = grailsAttributes.getApplicationContext().getResource("/").getFile().toString()
		def uploadFolder = baseFolder +'/upload/'
		MessageDigest digest = MessageDigest.getInstance("MD5")
		digest.update(now.toString().bytes);
		serialNo = new BigInteger(1, digest.digest()).toString(16).padLeft(32, '0')
		serialNo = serialNo.substring(16).toUpperCase()
		
		imgFile = serialNo + ext
		thumbnailFile = serialNo+"thumb" + ext
		
		
		def fn = uploadFolder + imgFile
		def fns = uploadFolder + thumbnailFile
		log.info("Base dir:" + baseFolder)
		log.info("file:" + fn)
 
		/*def imageTool = new ImageTool()
		imageTool.load(f.bytes)
		imageTool.thumbnail(240)
		imageTool.writeResult(fns, "JPEG")
		f.transferTo(new File(fn)) */
		//response.sendError(200, 'Done')
	}
	
	def getlist(){
		params.each {log.info(it)}
		log.info("GetList called")
		def mylist = params.list("Ids")
		
		def selected = Cert.getAll(mylist)
 
		def webRootDir = servletContext.getRealPath("/")
		def file = new File( "${webRootDir}".toString() + File.separatorChar + "upload" + File.separatorChar + "test.csv" )
		
		file.write("")
		for(cert in selected){
			log.info("cert name:" + cert.name +",serial:"+ cert.serial)
			def line = "${cert.serial},${cert.name},${cert.description},${cert.expert},${cert.dateTime}\n"
			file.append(line)
	
		}
		
		//redirect(action: "list")
		response.setHeader "Content-disposition", "attachment; filename=${file.name}"
		response.contentType = 'text/csv'
		response.outputStream << file.text
		response.outputStream.flush()
	}
	
	def upload2() {
		imgFile = null
		def f = request.getFile('myFile')
		def fo = f.getOriginalFilename()
 
		long now = java.lang.System.currentTimeMillis()
		log.info("Now:" + now)
		//fn =
		
		def baseFolder = grailsAttributes.getApplicationContext().getResource("/").getFile().toString()
		def uploadFolder = baseFolder +'/upload/'
		MessageDigest digest = MessageDigest.getInstance("MD5")
		digest.update(now.toString().bytes);
		serialNo = new BigInteger(1, digest.digest()).toString(16).padLeft(32, '0')
		serialNo = serialNo.substring(16).toUpperCase()
		
		imgFile = serialNo + ".jpg"
		thumbnailFile = serialNo+"thumb" + ".jpg"
		
		
		def fn = uploadFolder + imgFile
		def fns = uploadFolder + thumbnailFile
		log.info("Base dir:" + baseFolder)
		log.info("file:" + fn)

		/*
		def imageTool = new ImageTool()
		imageTool.load(f.bytes)
		//imageTool.thumbnail(240)
		//imageTool.writeResult(fns, "JPEG")
		f.transferTo(new File(fn))
		//response.sendError(200, 'Done')*/
	}
 
 
}
