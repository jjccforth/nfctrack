package co.micromacro.nfctrack

class DefaultController {

	def springSecurityService
	def userManagementService
	
	def someAction = { def user = springSecurityService.currentUser 
		

	}
	
    def index() {
	  boolean res = springSecurityService.isLoggedIn()
	  log.warn("login: $res")	
	  if(!springSecurityService.isLoggedIn()){
		  redirect(uri:"/login")
	  	return
	  }
	  def user = springSecurityService.currentUser
	  userManagementService.currentUser = user
	  log.info("CurrentUser set...")
      def principal = springSecurityService.principal
	  def roles = principal.authorities*.authority
	  Long uid = user.id
      //String username = principal.username
	  if (roles.any { it == "ROLE_ADMIN" }) {
 			//redirect(controller: "user", action: "list")
		  redirect(controller: "target", action: "l")
	  }
	  else if (roles.any { it == "ROLE_USER" })
			//redirect(controller: "user", action: "show",params: [id: uid])
	  		redirect(controller: "target", action: "l")
	  else
	  	redirect(uri:"/login")
	  	
	}
	
	def profile() {
		def user = springSecurityService.currentUser
		Long uid = user.id
		redirect(controller: "user", action: "show",params: [id: uid])
	
	}
}
