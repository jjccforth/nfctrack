
<%@ page import="co.micromacro.nfctrack.Target" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'target.label', default: '追踪人员')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-target" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="l"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<sec:ifAnyGranted roles="ROLE_MANAGER,ROLE_ADMIN">
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
				</sec:ifAnyGranted>
			</ul>
		</div>
		<div id="show-target" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list target">
			
				<g:if test="${targetInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="target.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${targetInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${targetInstance?.contactNumber1}">
				<li class="fieldcontain">
					<span id="contactNumber1-label" class="property-label"><g:message code="target.contactNumber1.label" default="Contact Number" /></span>
					
						<span class="property-value" aria-labelledby="contactNumber1-label"><g:fieldValue bean="${targetInstance}" field="contactNumber1"/></span>
					
				</li>
				</g:if>

				<g:if test="${targetInstance?.contactPerson1}">
				<li class="fieldcontain">
					<span id="contactPerson1-label" class="property-label"><g:message code="target.contactPerson1.label" default="Contact Person1" /></span>
					
						<span class="property-value" aria-labelledby="contactPerson1-label"><g:fieldValue bean="${targetInstance}" field="contactPerson1"/></span>
					
				</li>
				</g:if>
				
				<g:if test="${targetInstance?.relation1}">
				<li class="fieldcontain">
					<span id="contactPerson1-label" class="property-label"><g:message code="target.relation1.label" default="Relation1" /></span>
					
						<span class="property-value" aria-labelledby="contactPerson1-label"><g:fieldValue bean="${targetInstance}" field="relation1"/></span>
					
				</li>
				</g:if>

				
				<g:if test="${targetInstance?.contactNumber2}">
				<li class="fieldcontain">
					<span id="contactNumber2-label" class="property-label"><g:message code="target.contactNumber2.label" default="Contact Number2" /></span>
					
						<span class="property-value" aria-labelledby="contactNumber2-label"><g:fieldValue bean="${targetInstance}" field="contactNumber2"/></span>
					
				</li>
				</g:if>
				
				<g:if test="${targetInstance?.contactPerson2}">
				<li class="fieldcontain">
					<span id="contactPerson2-label" class="property-label"><g:message code="target.contactPerson2.label" default="Contact Person2" /></span>
						<span class="property-value" aria-labelledby="contactPerson2-label"><g:fieldValue bean="${targetInstance}" field="contactPerson2"/></span>
				</li>
				</g:if>
				
				<g:if test="${targetInstance?.relation2}">
				<li class="fieldcontain">
					<span id="relation2-label" class="property-label"><g:message code="target.relation1.label" default="Relation1" /></span>
					
						<span class="property-value" aria-labelledby="relation2-label"><g:fieldValue bean="${targetInstance}" field="relation2"/></span>
					
				</li>
				</g:if>
				
				<!--  -->
			
				<g:if test="${targetInstance?.homeAddress}">
				<li class="fieldcontain">
					<span id="homeAddress-label" class="property-label"><g:message code="target.homeAddress.label" default="Home Address" /></span>
					
						<span class="property-value" aria-labelledby="homeAddress-label"><g:fieldValue bean="${targetInstance}" field="homeAddress"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${targetInstance?.communityCenter}">
				<li class="fieldcontain">
					<span id="communityCenter-label" class="property-label"><g:message code="target.communityCenter.label" default="Community Center" /></span>
					
						<span class="property-value" aria-labelledby="communityCenter-label"><g:fieldValue bean="${targetInstance}" field="communityCenter"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${targetInstance?.bandId}">
				<li class="fieldcontain">
					<span id="bandId-label" class="property-label"><g:message code="target.bandId.label" default="Band Id" /></span>
					
						<span class="property-value" aria-labelledby="bandId-label"><g:fieldValue bean="${targetInstance}" field="bandId"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${targetInstance?.city}">
				<li class="fieldcontain">
					<span id="city-label" class="property-label"><g:message code="target.city.label" default="City" /></span>
					
						<span class="property-value" aria-labelledby="city-label"><g:link controller="city" action="show" id="${targetInstance?.city?.id}">${targetInstance?.city?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${targetInstance?.comment}">
				<li class="fieldcontain">
					<span id="comment-label" class="property-label"><g:message code="target.comment.label" default="Comment" /></span>
					
						<span class="property-value" aria-labelledby="comment-label"><g:fieldValue bean="${targetInstance}" field="comment"/></span>
					
				</li>
				</g:if>
			

			
				<g:if test="${targetInstance?.dateOfBirth}">
				<li class="fieldcontain">
					<span id="dateOfBirth-label" class="property-label"><g:message code="target.dateOfBirth.label" default="Date Of Birth" /></span>
					
						<span class="property-value" aria-labelledby="dateOfBirth-label"><g:formatDate format="yyyy-MM-dd" date="${targetInstance?.dateOfBirth}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${targetInstance?.dateOfRegister}">
				<li class="fieldcontain">
					<span id="dateOfRegister-label" class="property-label"><g:message code="target.dateOfRegister.label" default="Date Of Register" /></span>
					
						<span class="property-value" aria-labelledby="dateOfRegister-label"><g:formatDate format="yyyy-MM-dd" date="${targetInstance?.dateOfRegister}" /></span>
					
				</li>
				</g:if>
			

				<li class="fieldcontain">
					<span id="genderIsMale-label" class="property-label"><g:message code="target.genderIsMale.label" default="Gender Is Male" /></span>
					
					
					<g:if test="${targetInstance?.genderIsMale}">
						<span class="property-value" aria-labelledby="genderIsMale-label">男</span>
						</g:if>
						<g:else>
						<span class="property-value" aria-labelledby="genderIsMale-label">女</span>
						</g:else>
						
					
				</li>

			
				<g:if test="${targetInstance?.residentCardNo}">
				<li class="fieldcontain">
					<span id="residentCardNo-label" class="property-label"><g:message code="target.residentCardNo.label" default="Resident Card No" /></span>
					
						<span class="property-value" aria-labelledby="residentCardNo-label"><g:fieldValue bean="${targetInstance}" field="residentCardNo"/></span>
					
				</li>
				</g:if>
				<li class="fieldcontain">
				<div class="control-group">
					<span class="property-label"><g:message code="thing.image.label" default="照片" /></span>
					<span class="property-value">
						<g:if test="${targetInstance?.imgFile}">
						
							<img  width=240 src="${request.contextPath}/upload/${targetInstance?.imgFile}" alt="Image">
						
						</g:if>
					</span>
				</div>
				</li>					
			
			</ol>
			<sec:ifAnyGranted roles="ROLE_MANAGER,ROLE_ADMIN">
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${targetInstance?.id}" />
					<g:link class="edit" action="edit" id="${targetInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
			</sec:ifAnyGranted>
		</div>
	</body>
</html>
