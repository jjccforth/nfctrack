
<%@ page import="co.micromacro.nfctrack.Target" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'target.label', default: 'Target')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-target" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="l"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-target" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list target">
			
				<g:if test="${targetInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="target.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${targetInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${targetInstance?.contactNumber}">
				<li class="fieldcontain">
					<span id="contactNumber-label" class="property-label"><g:message code="target.contactNumber.label" default="Contact Number" /></span>
					
						<span class="property-value" aria-labelledby="contactNumber-label"><g:fieldValue bean="${targetInstance}" field="contactNumber"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${targetInstance?.homeAddress}">
				<li class="fieldcontain">
					<span id="homeAddress-label" class="property-label"><g:message code="target.homeAddress.label" default="Home Address" /></span>
					
						<span class="property-value" aria-labelledby="homeAddress-label"><g:fieldValue bean="${targetInstance}" field="homeAddress"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${targetInstance?.communityCenter}">
				<li class="fieldcontain">
					<span id="communityCenter-label" class="property-label"><g:message code="target.communityCenter.label" default="Community Center" /></span>
					
						<span class="property-value" aria-labelledby="communityCenter-label"><g:fieldValue bean="${targetInstance}" field="communityCenter"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${targetInstance?.bandId}">
				<li class="fieldcontain">
					<span id="bandId-label" class="property-label"><g:message code="target.bandId.label" default="Band Id" /></span>
					
						<span class="property-value" aria-labelledby="bandId-label"><g:fieldValue bean="${targetInstance}" field="bandId"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${targetInstance?.city}">
				<li class="fieldcontain">
					<span id="city-label" class="property-label"><g:message code="target.city.label" default="City" /></span>
					
						<span class="property-value" aria-labelledby="city-label"><g:link controller="city" action="show" id="${targetInstance?.city?.id}">${targetInstance?.city?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${targetInstance?.comment}">
				<li class="fieldcontain">
					<span id="comment-label" class="property-label"><g:message code="target.comment.label" default="Comment" /></span>
					
						<span class="property-value" aria-labelledby="comment-label"><g:fieldValue bean="${targetInstance}" field="comment"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${targetInstance?.contactPerson}">
				<li class="fieldcontain">
					<span id="contactPerson-label" class="property-label"><g:message code="target.contactPerson.label" default="Contact Person" /></span>
					
						<span class="property-value" aria-labelledby="contactPerson-label"><g:fieldValue bean="${targetInstance}" field="contactPerson"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${targetInstance?.dateOfBirth}">
				<li class="fieldcontain">
					<span id="dateOfBirth-label" class="property-label"><g:message code="target.dateOfBirth.label" default="Date Of Birth" /></span>
					
						<span class="property-value" aria-labelledby="dateOfBirth-label"><g:formatDate date="${targetInstance?.dateOfBirth}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${targetInstance?.dateOfRegister}">
				<li class="fieldcontain">
					<span id="dateOfRegister-label" class="property-label"><g:message code="target.dateOfRegister.label" default="Date Of Register" /></span>
					
						<span class="property-value" aria-labelledby="dateOfRegister-label"><g:formatDate date="${targetInstance?.dateOfRegister}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${targetInstance?.genderIsMale}">
				<li class="fieldcontain">
					<span id="genderIsMale-label" class="property-label"><g:message code="target.genderIsMale.label" default="Gender Is Male" /></span>
					
						<span class="property-value" aria-labelledby="genderIsMale-label"><g:formatBoolean boolean="${targetInstance?.genderIsMale}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${targetInstance?.residentCardNo}">
				<li class="fieldcontain">
					<span id="residentCardNo-label" class="property-label"><g:message code="target.residentCardNo.label" default="Resident Card No" /></span>
					
						<span class="property-value" aria-labelledby="residentCardNo-label"><g:fieldValue bean="${targetInstance}" field="residentCardNo"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${targetInstance?.id}" />
					<g:link class="edit" action="edit" id="${targetInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
