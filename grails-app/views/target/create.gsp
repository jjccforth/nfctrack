<%@ page import="co.micromacro.nfctrack.Target" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'target.label', default: '追踪人员')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#create-target" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="l"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="create-target" class="content scaffold-create" role="main">
			<h1><g:message code="default.create.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${targetInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${targetInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:uploadForm action="save"  class="form-horizontal">			
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="form">
					<div class="control-group">
						<label class="control-label">
						<g:message code="图像上传" default="图像上传" />
						</label>
	
						<div class="controls"><span class="input-xlarge focused">
							<input type="file" name="myFile" /></span>
						</div>
					</div>
				</fieldset>				
				<fieldset class="buttons">
					<g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				</fieldset>
			</g:uploadForm>				

		</div>
	</body>
</html>
