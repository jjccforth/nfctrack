
<%@ page import="co.micromacro.nfctrack.Target" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'target.label', default: '追踪人员')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-target" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_MANAGER">
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
				</sec:ifAnyGranted>
			</ul>
		</div>
		<div id="list-target" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="name" title="${message(code: 'target.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="contactNumber1" title="${message(code: 'target.contactNumber1.label', default: 'Contact Number1')}" />
					
						<g:sortableColumn property="homeAddress" title="${message(code: 'target.homeAddress.label', default: 'Home Address')}" />
					
						<g:sortableColumn property="bandId" title="${message(code: 'target.bandId.label', default: 'Band ID')}" />
					
						<g:sortableColumn property="shortCode" title="${message(code: 'target.shortCode.label', default: '芯片编号信息')}" />
					
						<th><g:message code="target.accessCount.label" default="计数" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${targetInstanceList}" status="i" var="targetInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${targetInstance.id}">${fieldValue(bean: targetInstance, field: "name")}</g:link></td>
					
						<td>${fieldValue(bean: targetInstance, field: "contactNumber1")}</td>
					
						<td>${fieldValue(bean: targetInstance, field: "homeAddress")}</td>
					
						<td>${fieldValue(bean: targetInstance, field: "bandId")}</td>
					
						<td>${fieldValue(bean: targetInstance, field: "shortCode")}</td>
					
						<td>${fieldValue(bean: targetInstance, field: "accessCount")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${targetInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
