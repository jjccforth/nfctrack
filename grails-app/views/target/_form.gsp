<%@ page import="co.micromacro.nfctrack.Target" %>



<div class="fieldcontain ${hasErrors(bean: targetInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="target.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${targetInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: targetInstance, field: 'contactNumber1', 'error')} required">
	<label for="contactNumber1">
		<g:message code="target.contactNumber1.label" default="Contact Number1" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="contactNumber1" required="" value="${targetInstance?.contactNumber1}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: targetInstance, field: 'contactPerson1', 'error')} ">
	<label for="contactPerson1">
		<g:message code="target.contactPerson1.label" default="Contact Person1" />
		
	</label>
	<g:textField name="contactPerson1" value="${targetInstance?.contactPerson1}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: targetInstance, field: 'relation1', 'error')} ">
	<label for="relation1">
		<g:message code="target.relation1.label" default="Relation" />
		
	</label>
	<g:textField name="relation1" value="${targetInstance?.relation1}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: targetInstance, field: 'contactNumber2', 'error')}">
	<label for="contactNumber2">
		<g:message code="target.contactNumber2.label" default="Contact Number2" />
	</label>
	<g:textField name="contactNumber2" value="${targetInstance?.contactNumber2}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: targetInstance, field: 'contactPerson2', 'error')} ">
	<label for="contactPerson2">
		<g:message code="target.contactPerson2.label" default="Contact Person2" />
		
	</label>
	<g:textField name="contactPerson2" value="${targetInstance?.contactPerson2}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: targetInstance, field: 'relation2', 'error')} ">
	<label for="relation2">
		<g:message code="target.relation1.label" default="Relation" />
		
	</label>
	<g:textField name="relation2" value="${targetInstance?.relation2}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: targetInstance, field: 'homeAddress', 'error')} required">
	<label for="homeAddress">
		<g:message code="target.homeAddress.label" default="Home Address" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="homeAddress" required="" value="${targetInstance?.homeAddress}" size ="40" />
</div>

<div class="fieldcontain ${hasErrors(bean: targetInstance, field: 'communityCenter', 'error')} required">
	<label for="communityCenter">
		<g:message code="target.communityCenter.label" default="Community Center" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="communityCenter" required="" value="${targetInstance?.communityCenter}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: targetInstance, field: 'bandId', 'error')} ">
	<label for="bandId">
		<g:message code="target.bandId.label" default="Band Id" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="bandId" required="" value="${targetInstance?.bandId}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: targetInstance, field: 'city', 'error')} required">
	<label for="city">
		<g:message code="target.city.label" default="City" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="city" name="city.id" from="${co.micromacro.nfctrack.City.list()}" optionKey="id" required="" value="${targetInstance?.city?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: targetInstance, field: 'comment', 'error')} ">
	<label for="comment">
		<g:message code="target.comment.label" default="Comment" />
		
	</label>
	<g:textArea name="comment" value="${targetInstance?.comment}" rows="4"/>
</div>



<div class="fieldcontain ${hasErrors(bean: targetInstance, field: 'dateOfBirth', 'error')} required">
	<label for="dateOfBirth">
		<g:message code="target.dateOfBirth.label" default="Date Of Birth" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="dateOfBirth" precision="day"  value="${targetInstance?.dateOfBirth}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: targetInstance, field: 'dateOfRegister', 'error')} required">
	<label for="dateOfRegister">
		<g:message code="target.dateOfRegister.label" default="Date Of Register" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="dateOfRegister" precision="day"  value="${targetInstance?.dateOfRegister}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: targetInstance, field: 'genderIsMale', 'error')} ">
	<label for="genderIsMale">
		<g:message code="target.genderIsMale.label" default="Gender Is Male" />
		
	</label>

	<g:radioGroup name="genderIsMale" labels="['男','女']" values="['true','false']" value="${targetInstance?.genderIsMale}">
			<span>${it.radio} ${it.label}</span>
	</g:radioGroup>
</div>

<div class="fieldcontain ${hasErrors(bean: targetInstance, field: 'residentCardNo', 'error')} ">
	<label for="residentCardNo">
		<g:message code="target.residentCardNo.label" default="Resident Card No" />
		
	</label>
	<g:textField name="residentCardNo" value="${targetInstance?.residentCardNo}"/>
</div>

