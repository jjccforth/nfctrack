<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!DOCTYPE html> 
<html>
  <head>
    <link rel="stylesheet" href="${resource(dir:'css',file:'jquery.mobile-1.2.0.min.css')}"/>
    <link rel="stylesheet" href="${resource(dir:'css',file:'main_m.css')}" type="text/css" media="all"/>		<title><g:layoutTitle default="手环"/>NFC智能手环系统</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		
		<script src="${resource(dir:'js',file:'jquery-1.8.2.min.js')}" /></script>
    	<script src="${resource(dir:'js',file:'jquery.mobile-1.2.0.min.js')}"></script>
		
		
		
		<g:layoutHead/>
		<r:layoutResources />
	</head>
	<body>
		<g:layoutBody/>
		<div class="footer" role="contentinfo"></div>
		<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
		<g:javascript library="application"/>
		<r:layoutResources />
	</body>
</html>
