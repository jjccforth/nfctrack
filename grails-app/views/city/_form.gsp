<%@ page import="co.micromacro.nfctrack.City" %>



<div class="fieldcontain ${hasErrors(bean: cityInstance, field: 'city', 'error')} ">
	<label for="city">
		<g:message code="city.city.label" default="City" />
		
	</label>
	<g:textField name="city" value="${cityInstance?.city}"/>
</div>

