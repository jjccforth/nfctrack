<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>华融丰瑞</title>
		<style type="text/css" media="screen">
			#status {
				background-color: #eee;
				border: .2em solid #fff;
				margin: 2em 2em 1em;
				padding: 1em;
				width: 12em;
				float: left;
				-moz-box-shadow: 0px 0px 1.25em #ccc;
				-webkit-box-shadow: 0px 0px 1.25em #ccc;
				box-shadow: 0px 0px 1.25em #ccc;
				-moz-border-radius: 0.6em;
				-webkit-border-radius: 0.6em;
				border-radius: 0.6em;
			}

			.ie6 #status {
				display: inline; /* float double margin fix http://www.positioniseverything.net/explorer/doubled-margin.html */
			}

			#status ul {
				font-size: 0.9em;
				list-style-type: none;
				margin-bottom: 0.6em;
				padding: 0;
			}

			#status li {
				line-height: 1.3;
			}

			#status h1 {
				text-transform: uppercase;
				font-size: 1.1em;
				margin: 0 0 0.3em;
			}

			#page-body {
				margin: 2em 1em 1.25em 18em;
			}

			h2 {
				margin-top: 1em;
				margin-bottom: 0.3em;
				font-size: 1em;
			}

			p {
				line-height: 1.5;
				margin: 0.25em 0;
			}

			#controller-list ul {
				list-style-position: inside;
			}

			#controller-list li {
				line-height: 1.3;
				list-style-position: inside;
				margin: 0.25em 0;
			}

			@media screen and (max-width: 480px) {
				#status {
					display: none;
				}

				#page-body {
					margin: 0 1em 1em;
				}

				#page-body h1 {
					margin-top: 0;
				}
			}
		</style>
	</head>
	<body>
		<a href="#page-body" class="skip"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div id="status" role="complementary">
			<h1>状态显示</h1>
			<ul>
				<li>版本号: <g:meta name="app.version"/></li>
				<li>NFC追踪探测次数: 3</li>
				<li>派发腕带数: ${co.micromacro.nfctrack.Target.count()}</li>
			</ul>
			<sec:ifAnyGranted roles="ROLE_ADMIN">
			<h1><g:link controller='user' action='list'>用户管理</g:link></h1>
			<h1><g:link controller='userRole' action='list'>角色管理</g:link></h1>
			</sec:ifAnyGranted>
			<ul>

			</ul>
		</div>
		<div id="page-body" role="main">
			<h1>欢迎进入NFC智能手环网页</h1>
			<p>NFC智能手环采用NFC和智能手机技术，将信息嵌入到芯片中</p>

			<div style="align:center;">
				<img alt="NFC腕带" src="${resource(dir: 'images', file: 'nfcband.jpg')}">
			</div>
			<div id="controller-list" role="navigation">
				<h2>点击进入:<g:link controller="target" class="list" action="l">管理界面</g:link></h2>

			</div>
		</div>
	</body>
</html>
