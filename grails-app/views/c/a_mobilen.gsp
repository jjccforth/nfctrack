
<%@ page import="co.micromacro.nfctrack.Target" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main_mobile">
		<g:set var="entityName" value="${message(code: 'target.label', default: 'Target')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
	<div data-role="page">
				
			<div data-role="header" data-nobackbtn="true" data-position="fixed">
			    <g:link action="a" id="${fakeId}" data-icon="arrow-l">
					<g:message code="default.nav.back.label" default="返回" />
			    </g:link>
				<h2><g:message code="default.detailinfo.label" default="详细信息" /></h2>

			</div>
		<div id="show-target" class="content scaffold-show" role="main">
			<h1><g:message code="default.detailinfo.label" default="详细信息" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>

						<g:if test="${targetInstance?.imgFile}">
						<center>
							<img  width=180 src="${request.contextPath}/upload/${targetInstance?.thumnail}" alt="Image">
						</center>
						</g:if>							

				<ul data-role="listview">
				
					<g:if test="${targetInstance?.name}">
				<li class="fieldcontain">
					<div class="ui-grid-b">
					<span id="name-label" class="property-label ui-block-a" style="width:30%"><g:message code="target.name.label" default="Name" /></span>
					
						<span class="property-value ui-block-b" style="width:60%" aria-labelledby="name-label"><g:fieldValue bean="${targetInstance}" field="name"/></span>
					</div>
				</li>
				</g:if>		
							
					
				<g:if test="${targetInstance?.homeAddress}">
				<li class="fieldcontain">
				<div class="ui-grid-b">
					<span id="homeAddress-label" class="property-label ui-block-a" style="width:30%"><g:message code="target.homeAddress.label" default="Home Address" /></span>
					
						<span class="property-value ui-block-b" style="width:60%" aria-labelledby="homeAddress-label"><g:fieldValue bean="${targetInstance}" field="homeAddress"/></span>
					</div>
				</li>
				</g:if>
			
	
			
				<g:if test="${targetInstance?.city}">
				<li class="fieldcontain">
				<div class="ui-grid-b">
					<span id="city-label" class="property-label ui-block-a" style="width:30%"><g:message code="target.city.label" default="City" /></span>
					
						<span class="property-value ui-block-b" style="width:60%" aria-labelledby="city-label">${targetInstance?.city?.encodeAsHTML()}</span>
					</div>
				</li>
				</g:if>
			
				<g:if test="${targetInstance?.comment}">
				<li class="fieldcontain">
				<div class="ui-grid-b">
					<span id="comment-label" class="property-label ui-block-a" style="width:30%"><g:message code="" default="" />病史及说明</span>
					
						<span class="property-value ui-block-b" style="width:60%" aria-labelledby="comment-label"><g:fieldValue bean="${targetInstance}" field="comment"/></span>
					</div>
				</li>
				</g:if>
	

				<g:if test="${targetInstance?.dateOfBirth}">
				<li class="fieldcontain">
					<div class="ui-grid-b">
					<span id="dateOfBirth-label" class="property-label ui-block-a" style="width:30%"><g:message code="target.dateOfBirth.label" default="Date Of Birth" /></span>
					
						<span class="property-value ui-block-b" style="width:60%" aria-labelledby="dateOfBirth-label"><g:formatDate  format="yyyy-MM-dd" date="${targetInstance?.dateOfBirth}" /></span>
					</div>
				</li>
				</g:if>
			

			
				<g:if test="${targetInstance?.genderIsMale}">
				<li class="fieldcontain">
					<div class="ui-grid-b">
					<span id="genderIsMale-label" class="property-label ui-block-a" style="width:30%"><g:message code="target.genderIsMale.label" default="Gender Is Male" /></span>
					
						<span class="property-value" aria-labelledby="genderIsMale-label">
						<g:if test="${targetInstance?.genderIsMale}">
						男
						</g:if>
						<g:else>
						女
						</g:else>
						</span>
					
					</div>
				</li>
				</g:if>
			
				<g:if test="${targetInstance?.residentCardNo}">
				<li class="fieldcontain">
					<div class="ui-grid-b">
					<span id="residentCardNo-label" class="property-label ui-block-a" style="width:30%"><g:message code="target.residentCardNo.label" default="Resident Card No" /></span>
					
						<span class="property-value" aria-labelledby="residentCardNo-label"><g:fieldValue bean="${targetInstance}" field="residentCardNo"/></span>
					</div>
				</li>
				</g:if>
			
			</ul>
<!-- Photo -->

		</div>
	</div>
	</body>
</html>
