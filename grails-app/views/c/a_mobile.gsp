
<%@ page import="co.micromacro.nfctrack.Target" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main_mobile">
		<g:set var="entityName" value="${message(code: 'target.label', default: 'Target')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
	<div data-role="page">
				
			<div data-role="header" data-nobackbtn="true" data-position="fixed">
				<h1><g:message code="default.pleasecontact.label" default="NFC智能追踪手环" /></h1>
			</div>
		<div id="show-target" class="content scaffold-show" role="main">
		<table class="show-data">
		<tbody><tr><td>
			<h2><g:message code="default.contactinfo.label" default="" />
			请拨打:<a href="tel:12349">12349</a>热线，</br>
			手环编号:
			<span style="color:#f00">
			<g:fieldValue bean="${targetInstance}" field="bandId" />
			</span>
			</br>
			或者和以下联络人联系：
			</h2>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			</td></tr></tbody>
			</table>
			
				<table class="show-data">
                <tbody>

				<tr>
			
				<g:if test="${targetInstance?.contactNumber1}">
				
	
				<td class="td-label">联系电话1</td>
				<td class="td-value">
					<a href="tel:${targetInstance?.contactNumber2}">
						<g:fieldValue bean="${targetInstance}" field="contactNumber1"/>
					</a>	
				</td>
				</g:if>
				</tr>
				<tr>
				<g:if test="${targetInstance?.contactPerson1}">
					<td class="td-label">联系人(1)</td>
					<td class="td-value">
						<g:fieldValue bean="${targetInstance}" field="contactPerson1"/>
						<g:if test="${targetInstance?.relation1}">
						(手环携带者之: <g:fieldValue bean="${targetInstance}" field="relation1"/>)
						</g:if>		
					</td>
				</g:if>
				
				</tr>
				<tr>
				<g:if test="${targetInstance?.contactNumber2}">
				<td class="td-label">联系电话2</td>
				<td class="td-value"><a href="tel:${targetInstance?.contactNumber2}">
						<g:fieldValue bean="${targetInstance}" field="contactNumber2"/>
				</a>
				</td>
				</g:if>
				</tr>
				<tr>
				<g:if test="${targetInstance?.contactPerson2}">
					<td class="td-label">联系人(2)</td>
					<td class="td-value">
						<g:fieldValue bean="${targetInstance}" field="contactPerson2"/>
						<g:if test="${targetInstance?.relation2}">
						(手环携带者之: <g:fieldValue bean="${targetInstance}" field="relation2"/>)
						</g:if>							
					</td>
				</g:if>
				</tr>

				</table>

				<ul data-role="listview">
				
				<li class="ui-last-child">
				<g:link class="fieldcontain ui-btn-icon-right ui-icon-carat-r" action="anext" id="${fakeId}">
				手环携带者:<g:fieldValue bean="${targetInstance}" field="name"/>，
${age}岁。更多信息...</g:link>
					
				</li>	
		

			
			</ul>

		</div>
	</div>
	</body>
</html>
