package co.micromacro.nfctrack

class City {
	
	String city;

    static constraints = {
		blank: false
    }
	
	String toString() {
		return city;
	}
}
