package co.micromacro.nfctrack

class Target {
	
	static belongsTo = [city: City]
	static transients = [ "shortCode" ]
	
	String  name
	String  residentCardNo
	String  bandId
	boolean genderIsMale //true for male, false for female
	Date	dateOfBirth
	Date	dateOfRegister
	String  contactNumber1
	String  contactPerson1
	String	relation1
	String  contactNumber2
	String  contactPerson2
	String	relation2
	String  homeAddress
	String  communityCenter
	String  comment
	String  shortCode
	String  imgFile
	Integer accessCount

	String getThumnail() {
		def imgf = imgFile.split("\\.")
		
		log.info( imgf[0] +":" + imgf[1])
		return imgf[0]+"thumb."+imgf[1]
	}
	
	static constraints = {
		name blank: false
		contactNumber1 blank: false
		homeAddress blank: false
		communityCenter blank: false
		
		}
}
