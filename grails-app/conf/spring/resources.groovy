import co.micromacro.nfctrack.UserPasswordEncoderListener
import co.micromacro.nfctrack.ValuesBeanImpl

// Place your Spring DSL code here
beans = {

    userPasswordEncoderListener(UserPasswordEncoderListener)

    localeResolver(org.springframework.web.servlet.i18n.SessionLocaleResolver) {
        defaultLocale = new Locale("zh","CN")
        java.util.Locale.setDefault(defaultLocale)
    }

    valueBean(ValuesBeanImpl) {
        someProperty = 42
        otherProperty = "blue"
        rotTarget = [3216549870,1234567889]
        rotContainer = [11111,54321]
    }

}
